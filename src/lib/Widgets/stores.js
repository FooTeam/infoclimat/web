import { localWritable } from '$lib/localStorage';
import { writable } from 'svelte/store';

export const precipitations = localWritable('precipitations', {
	period: 'daily',
	aggregate: 'sum',
	data: {
		labels: [],
		datasets: []
	}
});

export const minMaxTemperatures = localWritable('minMaxTemperatures', {
	period: 'daily',
	data: {
		labels: [],
		datasets: []
	}
});

export const periodSelector = localWritable('period', 'daily');
export const periodConfig = writable({
	day: 'numeric',
	month: 'long',
	year: 'numeric'
});
export const periodKeys = writable(['annee', 'mois', 'jour']);

export const dashboardEvents = localWritable('dashboardEvents', {
	period: 'daily',
	data: []
});

export const dashboardExtremes = localWritable('dashboardExtremes', []);
