import { localWritable } from '$lib/localStorage';

export const user = localWritable('user');

export const login = (callback = () => {}) => {
	user.set(true);
	callback();
};

const createHistory = () => {
	// use station id as key for uniqueness (Set can't be used in localStorage)
	const { subscribe, set, update } = localWritable('stationHistory', {});

	const add = (station) => {
		update((history) => {
			if (Object.keys(history).length > 6) {
				delete history[Object.keys(history)[0]];
			}

			history[station.id] = station;
			return history;
		});
	};

	const reset = () => {
		set({});
	};

	return { reset, subscribe, add };
};

export const stationHistory = createHistory();

export const dashboardStation = localWritable('dashboardStation', undefined);
