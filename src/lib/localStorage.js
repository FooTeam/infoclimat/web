/*
 *  Svelte localStorage
 *  Copyright (C) 2021 FooTeam
 */
import { get, writable } from 'svelte/store';

const getInitialValue = (initialValue) =>
	typeof initialValue === 'function' ? initialValue() : initialValue;

const getValue = (key, initialValue) => {
	const json = localStorage.getItem(key);

	if (json && json !== 'undefined') {
		return JSON.parse(json);
	}

	return getInitialValue(initialValue);
};

export const localWritable = (key, initialValue, start) => {
	if (typeof localStorage === 'undefined') {
		return writable(getInitialValue(initialValue));
	}

	const value = getValue(key, initialValue);
	const browserWritable = start ? writable(value, () => start(localSet)) : writable(value);

	localStorage.setItem(key, JSON.stringify(value));
	const { subscribe, set } = browserWritable;

	const localSet = (value) => {
		localStorage.setItem(key, JSON.stringify(value));
		set(value);
	};
	const localUpdate = (callback) => {
		localSet(callback(get(browserWritable)));
	};

	return {
		set: localSet,
		update: localUpdate,
		subscribe
	};
};
