import { writable, get } from 'svelte/store';
import { onMount } from 'svelte';

export const events = writable([], (set) => {
	onMount(async () => {
		const result = await fetch(`https://api.infoclimat.localhost/api/historic`).then(
			async (res) => {
				return await res.json();
			}
		);
		set(result);
	});

	return () => {};
});

export const fetchEvent = async (id) => {
	const event = await fetch(`https://api.infoclimat.localhost/api/historic/${id}`).then(
		async (res) => {
			return await res.json()[0];
		}
	);
	let store = get(events);
	Object.keys(store).forEach((key) => {
		if (store[key].id === id) {
			store[key] = event;
		}
	});
	events.set(store);
};
