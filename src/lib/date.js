export function dateFromParts(parts, options = {}) {
	return new Date(parts.join('-')).toLocaleDateString('fr-FR', options);
}

export function strToDate(string, options = {}) {
	return new Date(string).toLocaleDateString('fr-FR', options);
}
