import { writable } from 'svelte/store';
import { onMount } from 'svelte';

export const stations = writable([], (set) => {
	onMount(async () => {
		let result = await fetch('https://api.infoclimat.localhost/api/live').then(async (res) => {
			return await res.json();
		});
		set(result);
	});

	return () => {};
});
