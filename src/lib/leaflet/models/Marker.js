export default class Marker {
	constructor(lat, lng, title, tx, tn, rr) {
		this.lat = lat;
		this.lng = lng;
		this.title = title;
		this.tx = tx;
		this.tn = tn;
		this.rr = rr;
	}

	getIcon() {
		let icon = {
			iconUrl: 'assets/icons/rain.svg',
			iconSize: [48, 48],
			iconAnchor: [12, 41],
			popupAnchor: [1, -34],
			tooltipAnchor: [1, -34]
		};
		if (this.rr <= 5) {
			icon.iconUrl = 'assets/icons/sun.svg';
		} else if (this.rr <= 15) {
			icon.iconUrl = 'assets/icons/cloudy.svg';
		}
		return icon;
	}
}
