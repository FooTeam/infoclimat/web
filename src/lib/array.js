export function pluck(array, key) {
	return array.map((item) => item[key]);
}

export function only(array, keys) {
	return array.map((item) => keys.map((key) => item[key]));
}

export function except(array, keys) {
	return array.map((item) =>
		Object.keys(item)
			.filter((key) => !keys.includes(key))
			.map((key) => item[key])
	);
}
